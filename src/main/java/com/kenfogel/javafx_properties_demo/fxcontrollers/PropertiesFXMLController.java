package com.kenfogel.javafx_properties_demo.fxcontrollers;

import com.kenfogel.javafx_properties_demo.propertiesdemo.mailbean.MailConfigPropertiesFX;
import com.kenfogel.javafx_properties_demo.propertiesdemo.manager.PropertiesManager;
import java.io.IOException;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This is the controller for Scene.fxml
 *
 * @author Ken Fogel
 */
public class PropertiesFXMLController {

    // Real programmers use logging, not System.out.println
    private final static Logger LOG = LoggerFactory.getLogger(PropertiesFXMLController.class);

    // Injected fields
    @FXML
    private TextField name;

    @FXML
    private TextField emailAddress;

    @FXML
    private PasswordField password;

    // Resources are injected
    @FXML
    private ResourceBundle resources;

    Scene scene;
    Stage stage;
    RestOfProgramController rpc;

    private final MailConfigPropertiesFX mcp;

    /**
     * Default constructor creates an instance of FishData that can be bound to
     * the form
     */
    public PropertiesFXMLController() {
        super();
        mcp = new MailConfigPropertiesFX();
    }

    /**
     * Without the ability to pass values thru a constructor we need a set
     * method for any variables required in this class
     *
     * @param scene
     * @param stage
     * @param rpc
     */
    public void setSceneStageController(Scene scene, Stage stage, RestOfProgramController rpc) {
        this.scene = scene;
        this.stage = stage;
        this.rpc = rpc;
    }

    /**
     * The initialize method is used to handle bindings
     */
    @FXML
    private void initialize() {
        Bindings.bindBidirectional(name.textProperty(), mcp.userNameProperty());
        Bindings.bindBidirectional(emailAddress.textProperty(), mcp.emailAddressProperty());
        Bindings.bindBidirectional(password.textProperty(), mcp.passwordProperty());
    }

    /**
     * Event handler for Cancel button Exit the program
     */
    @FXML
    void onCancel(ActionEvent event) {
        Platform.exit();
    }

    /**
     * Event handler for Clear button
     *
     * @param event
     */
    @FXML
    void onClear(ActionEvent event) {
        mcp.setUserName("");
        mcp.setUserEmailAddress("");
        mcp.setPassword("");
    }

    /**
     * Event handler for Save button
     *
     * @param event
     */
    @FXML
    void onSave(ActionEvent event) {
        PropertiesManager pm = new PropertiesManager();
        try {
            pm.writeTextProperties("", "MailConfig", mcp);
            // Display properties in TextArea
            rpc.displayPropertiesInTextArea();
            // Change the scene on the stage
            stage.setScene(scene);
        } catch (IOException ex) {
            LOG.error("onSave error", ex);
            errorAlert("onSave()");
            Platform.exit();
        }
    }

    /**
     * Error message popup dialog
     *
     * @param msg
     */
    private void errorAlert(String msg) {
        Alert dialog = new Alert(Alert.AlertType.ERROR);
        dialog.setTitle(resources.getString("Error"));
        dialog.setHeaderText(resources.getString("Error"));
        dialog.setContentText(resources.getString(msg));
        dialog.show();
    }
}
