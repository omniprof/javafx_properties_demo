package com.kenfogel.javafx_properties_demo.fxcontrollers;

import com.kenfogel.javafx_properties_demo.propertiesdemo.mailbean.MailConfigPropertiesFX;
import com.kenfogel.javafx_properties_demo.propertiesdemo.manager.PropertiesManager;
import java.io.IOException;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TextArea;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RestOfProgramController {

    // Real programmers use logging, not System.out.println
    private final static Logger LOG = LoggerFactory.getLogger(RestOfProgramController.class);

    @FXML
    private TextArea textArea;

    // Resources are injected
    @FXML
    private ResourceBundle resources;

    @FXML
    public void initialize() {
    }

    @FXML
    void onExit(ActionEvent event) {
        Platform.exit();
    }

    public void displayPropertiesInTextArea() {
        PropertiesManager pm = new PropertiesManager();
        MailConfigPropertiesFX mcp = new MailConfigPropertiesFX();
        try {
            if (pm.loadTextProperties(mcp, "", "MailConfig")) {
                textArea.setText(mcp.toString());
            }
        } catch (IOException ex) {
            LOG.error("displayPropertiesInTextArea error", ex);
            errorAlert("displayPropertiesInTextArea()");
            Platform.exit();
        }
    }

    /**
     * Error message popup dialog
     *
     * @param msg
     */
    private void errorAlert(String msg) {
        Alert dialog = new Alert(Alert.AlertType.ERROR);
        dialog.setTitle(resources.getString("Error"));
        dialog.setHeaderText(resources.getString("Error"));
        dialog.setContentText(resources.getString(msg));
        dialog.show();
    }
}
