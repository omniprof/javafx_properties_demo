package com.kenfogel.javafx_properties_demo.propertiesdemo.manager;

import com.kenfogel.javafx_properties_demo.propertiesdemo.mailbean.MailConfigPropertiesFX;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import static java.nio.file.Files.newInputStream;
import static java.nio.file.Files.newOutputStream;
import java.nio.file.Path;
import static java.nio.file.Paths.get;
import java.util.Properties;

/**
 * Example of a class to manage properties
 *
 * @author Ken Fogel
 *
 */
public class PropertiesManager {

    /**
     * Updates a MailConfigBean object with the contents of the properties file
     *
     * @param mailConfig
     * @param path
     * @param propFileName
     * @return
     * @throws java.io.IOException
     */
    public final boolean loadTextProperties(final MailConfigPropertiesFX mailConfig, final String path, final String propFileName) throws IOException {

        boolean found = false;
        Properties prop = new Properties();

        Path txtFile = get(path, propFileName + ".properties");

        // File must exist
        if (Files.exists(txtFile)) {
            try (InputStream propFileStream = newInputStream(txtFile);) {
                prop.load(propFileStream);
            }
            mailConfig.setUserName(prop.getProperty("userName"));
            mailConfig.setPassword(prop.getProperty("password"));
            mailConfig.setUserEmailAddress(prop.getProperty("userEmailAddress"));

            found = true;
        }
        return found;
    }

    /**
     * Creates a plain text properties file based on the parameters
     *
     * @param path Must exist, will not be created
     * @param propFileName Name of the properties file
     * @param mailConfig The bean to store into the properties
     * @throws IOException
     */
    public final void writeTextProperties(final String path, final String propFileName, final MailConfigPropertiesFX mailConfig) throws IOException {

        Properties prop = new Properties();

        prop.setProperty("userName", mailConfig.getUserName());
        prop.setProperty("password", mailConfig.getPassword());
        prop.setProperty("userEmailAddress", mailConfig.getUserEmailAddress());

        Path txtFile = get(path, propFileName + ".properties");

        // Creates the file or if file exists it is truncated to length of zero
        // before writing
        try (OutputStream propFileStream = newOutputStream(txtFile)) {
            prop.store(propFileStream, "SMTP Properties");
        }
    }
}
