package com.kenfogel.javafx_properties_demo.propertiesdemo.mailbean;

import java.util.Objects;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * Property bean
 *
 * @author Ken Fogel
 */
public class MailConfigPropertiesFX {

    private final StringProperty userName;
    private final StringProperty userEmailAddress;
    private final StringProperty password;

    /**
     * Default Constructor
     */
    public MailConfigPropertiesFX() {
        this("", "", "");
    }

    /**
     * @param userEmailAddress
     * @param userName
     * @param password
     */
    public MailConfigPropertiesFX(final String userName, final String userEmailAddress, final String password) {
        super();
        this.userName = new SimpleStringProperty(userName);
        this.userEmailAddress = new SimpleStringProperty(userEmailAddress);
        this.password = new SimpleStringProperty(password);
    }

    public String getUserName() {
        return userName.get();
    }

    public String getUserEmailAddress() {
        return userEmailAddress.get();
    }

    public String getPassword() {
        return password.get();
    }

    public void setUserName(String s) {
        userName.set(s);
    }

    public void setUserEmailAddress(String s) {
        userEmailAddress.set(s);
    }

    public void setPassword(String s) {
        password.set(s);
    }

    public StringProperty userNameProperty() {
        return userName;
    }

    public StringProperty emailAddressProperty() {
        return userEmailAddress;
    }

    public StringProperty passwordProperty() {
        return password;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 17 * hash + Objects.hashCode(this.userName.get());
        hash = 17 * hash + Objects.hashCode(this.userEmailAddress.get());
        hash = 17 * hash + Objects.hashCode(this.password.get());
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final MailConfigPropertiesFX other = (MailConfigPropertiesFX) obj;
        if (!Objects.equals(this.userName.get(), other.userName.get())) {
            return false;
        }
        if (!Objects.equals(this.userEmailAddress.get(), other.userEmailAddress.get())) {
            return false;
        }
        return Objects.equals(this.password.get(), other.password.get());
    }

    @Override
    public String toString() {
        return "MailConfigProperties\n{\t" + "userName=" + userName.get() + "\n\tuserEmailAddress=" + userEmailAddress.get() + "\n\tpassword=" + password.get() + "\n}";
    }
}
