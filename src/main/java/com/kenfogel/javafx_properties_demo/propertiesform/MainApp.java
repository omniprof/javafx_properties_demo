package com.kenfogel.javafx_properties_demo.propertiesform;

import com.kenfogel.javafx_properties_demo.fxcontrollers.PropertiesFXMLController;
import com.kenfogel.javafx_properties_demo.fxcontrollers.RestOfProgramController;
import com.kenfogel.javafx_properties_demo.propertiesdemo.mailbean.MailConfigPropertiesFX;
import com.kenfogel.javafx_properties_demo.propertiesdemo.manager.PropertiesManager;
import java.io.IOException;
import java.util.ResourceBundle;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Demo of multiple scenes https://gitlab.com/omniprof/JavaFXPropertiesDemo.git
 *
 * @author Ken Fogel
 */
public class MainApp extends Application {

    private final static Logger LOG = LoggerFactory.getLogger(MainApp.class);
    
    private RestOfProgramController rpc;
    private Stage stage;

    /**
     * Default constructor
     */
    public MainApp() {
        super();
    }

    /**
     * Standard start method. Checks for presence of a properties file and if
     * not found then it will display the properties form.
     *
     * @param stage
     * @throws Exception
     */
    @Override
    public void start(Stage stage) throws Exception {
        this.stage = stage;
        Scene scene2 = createRestOfTheProgram();
        Scene scene1 = createPropertiesForm(scene2);
        scene1.getStylesheets().add("/styles/Styles.css");
        scene2.getStylesheets().add("/styles/restofprogram.css");
        if (!checkForProperties()) {
            this.stage.setScene(scene1);
        } else {
            this.stage.setScene(scene2);
        }

        this.stage.setTitle("Mail Config Form");

        this.stage.show();
    }

    /**
     * Create a stage with the scene showing the primary program window
     *
     * @return
     * @throws Exception
     */
    private Scene createRestOfTheProgram() throws Exception {
        // Instantiate the FXMLLoader
        FXMLLoader loader = new FXMLLoader();

        // Set the location of the fxml file in the FXMLLoader
        loader.setLocation(this.getClass().getResource("/fxml/RestOfProgram.fxml"));

        // Localize the loader with its bundle
        // Uses the default locale and if a matching bundle is not found
        // will then use MessagesBundle.properties
        loader.setResources(ResourceBundle.getBundle("MessagesBundle"));

        // Parent is the base class for all nodes that have children in the
        // scene graph such as AnchorPane and most other containers
        Parent root = (AnchorPane) loader.load();

        // Retrieve a reference to the controller so that we can call
        // upon a method in the controller
        rpc = loader.getController();
        rpc.displayPropertiesInTextArea();

        Scene scene = new Scene(root);
        scene.getStylesheets().add("/styles/Styles.css");
        return scene;
    }

    /**
     * Assuming that properties are not found then here is the properties window
     *
     * @param scene2
     * @throws Exception
     */
    private Scene createPropertiesForm(Scene scene2) throws Exception {
        // Instantiate the FXMLLoader
        FXMLLoader loader = new FXMLLoader();

        // Set the location of the fxml file in the FXMLLoader
        loader.setLocation(this.getClass().getResource("/fxml/Properties.fxml"));

        // Localize the loader with its bundle
        // Uses the default locale and if a matching bundle is not found
        // will then use MessagesBundle.properties
        loader.setResources(ResourceBundle.getBundle("MessagesBundle"));

        // Parent is the base class for all nodes that have children in the
        // scene graph such as AnchorPane and most other containers
        Parent root = (GridPane) loader.load();

        // Retrieve a reference to the controller so that we can pass
        // in the properties object
        PropertiesFXMLController controller = loader.getController();

        // Pass the references that the properties controller will need to
        // display the second scene after entering the mail config data
        controller.setSceneStageController(scene2, stage, rpc);

        Scene scene = new Scene(root);
        return scene;
    }

    /**
     * Check if a Properties file exists and can be loaded into a bean. This
     * does not verify that the contents of the bean fields are valid.
     *
     * @return found Have we been able to load the properties?
     */
    private boolean checkForProperties() {
        boolean found = false;
        MailConfigPropertiesFX mcp = new MailConfigPropertiesFX();
        PropertiesManager pm = new PropertiesManager();

        try {
            if (pm.loadTextProperties(mcp, "", "MailConfig")) {
                found = true;
            }
        } catch (IOException ex) {
            LOG.error("checkForProperties error", ex);
        }
        return found;
    }

    /**
     * The main() method
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
}
